from .factories import ClientFactory, ParkingFactory
from ..main.models import Client, Parking

def test_create_client_by_endpoint(client) -> None:
    '''
    Создание клиента через endpoint
    '''
    client_data = ClientFactory()
    resp = client.post("/clients", data=client_data.to_json())
    assert resp.status_code == 201

def test_create_parking_by_endpoint(client) -> None:
    '''
        Создание парковки через endpoint
    '''
    parking_data = ParkingFactory()
    resp = client.post("/parkings", data=parking_data.to_json())
    assert resp.status_code == 201


def test_create_client(app, db):
    client = ClientFactory()
    db.session.commit()
    assert client.id is not None
    assert len(db.session.execute(db.select(Client)).all()) == 2


def test_create_parking(app, db):
    parking = ParkingFactory()
    db.session.commit()
    assert parking.id is not None
    assert len(db.session.execute(db.select(Parking)).all()) == 2