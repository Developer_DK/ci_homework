import factory
import factory.fuzzy as fuzzy
import random

from ..main.app import db
from ..main.models import Client, Parking


class ClientFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Client
        sqlalchemy_session = db.session

    name = factory.Faker('first_name')
    surname = factory.Faker('last_name')
    credit_card = random.choice([None, fuzzy.FuzzyText(chars='pyint', length=16)])
    car_number = fuzzy.FuzzyText(length=10)


class ParkingFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Parking
        sqlalchemy_session = db.session

    address = factory.Faker('address')
    opened = fuzzy.FuzzyChoice([True, False])
    count_places = fuzzy.FuzzyInteger(10, 500)
    count_available_places = factory.LazyAttribute(lambda x: random.randrange(0, x.count_places))
