from datetime import datetime
import pytest
from ..main.app import create_app, db as _db
from ..main.database import Base
from ..main.models import Client, ClientParking, Parking



@pytest.fixture
def app():
    _app = create_app()
    _app.config["TESTING"] = True
    _app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"

    with _app.app_context():
        _db.create_all() 
        Base.metadata.drop_all(_db.engine)
        Base.metadata.create_all(_db.engine)
        client = Client(id=1,
                        name="Ivan",
                        surname="Petrov",
                        credit_card='0000 1111 2222',
                        car_number='A100PP50')

        parking = Parking(id=1,
                          address='Main street, 77 ',
                          opened=True,
                          count_places=100,
                          count_available_places=100)

        client_parking = ClientParking(id=1,
                                       client_id=1,
                                       parking_id=1,
                                       time_in=datetime.strptime('01.10.2023 23:00:00', '%d.%m.%Y %H:%M:%S'),
                                       time_out=None)

        _db.session.add(client)
        _db.session.add(parking)
        _db.session.add(client_parking)
        _db.session.commit()

        yield _app
        _db.session.close()
        _db.drop_all()


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client



@pytest.fixture
def db(app):
    with app.app_context():
        yield _db
