import pytest
from ..main.models import ClientParking, Parking


@pytest.mark.parametrize("route", ["/", "/clients/1", "/clients?client_id=1", "/parkings"])
def test_route_status(client, route):
    rv = client.get(route)
    assert rv.status_code == 200


def test_create_client(client) -> None:
    client_data = {"name": "Anna",
                   "surname": "Smirnova",
                   "credit_card": "8888 0000 4444",
                   "car_number": "H569TX55"}
    resp = client.post("/clients", data=client_data)

    assert resp.status_code == 201


def test_create_parking(client) -> None:
    parking_data = {"address": "Baker street, 43",
                    "opened": True,
                    "count_places": 120,
                    "count_available_places": 120}
    resp = client.post("/parkings", data=parking_data)

    assert resp.status_code == 201


@pytest.mark.parking
def test_client_parking(client, db) -> None:
    parking = db.session.get(Parking, 1)
    assert parking
    assert parking.opened
    assert parking.count_available_places > 0
    count_available_places = parking.count_available_places

    client_parking_data = {"client_id": 1,
                           "parking_id": 1
                           }
    resp = client.post("/client_parkings", data=client_parking_data)
    assert resp.status_code == 201
    parking = db.session.get(Parking, 1)
    assert count_available_places > parking.count_available_places


@pytest.mark.parking
def test_client_leave_parking(client, db) -> None:

    parking = db.session.get(Parking, 1)
    assert parking
    assert parking.count_available_places > 0
    count_available_places = parking.count_available_places

    client_parking = db.session.execute(db.select(ClientParking).filter_by(client_id=1, parking_id=1)).scalar_one()
    assert client_parking
    time_in = client_parking.time_in
    client_parking_id = client_parking.id

    client_parking_data = {"client_id": 1,
                           "parking_id": 1
                           }
    resp = client.delete("/client_parkings", data=client_parking_data)
    assert resp.status_code == 202
    client_parking = db.session.get(ClientParking, client_parking_id)
    assert time_in <= client_parking.time_out

    parking = db.session.get(Parking, 1)
    assert count_available_places < parking.count_available_places




