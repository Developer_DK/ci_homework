from datetime import datetime
from typing import List

from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def create_app():
    app = Flask(__name__)
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///hw.db"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    db.init_app(app)

    from .models import Client, ClientParking, Parking

    with app.app_context():
        db.create_all()

    @app.route("/", methods=["GET"])
    def get_test_handler():
        return "Hello! I'm parking API", 200

    @app.route("/clients", methods=["POST"])
    def create_client_handler():
        """Создание нового клиента"""
        name = request.form.get("name", type=str)
        surname = request.form.get("surname", type=str)
        credit_card = request.form.get("credit_card", type=str)
        car_number = request.form.get("car_number", type=str)

        new_client = Client(
            name=name, surname=surname, credit_card=credit_card, car_number=car_number
        )

        db.session.add(new_client)
        db.session.commit()

        return "", 201

    @app.route("/clients", methods=["GET"])
    def get_clients_handler():
        """Получение списка клиентов"""
        clients: List[Client] = db.session.execute(db.select(Client)).scalars()
        clients_list = [u.to_json() for u in clients]
        return jsonify(clients_list), 200

    @app.route("/clients/<int:client_id>", methods=["GET"])
    def get_client_handler(client_id: int):
        """Получение клиента по ид"""
        client: Client = db.session.execute(
            db.select(Client).filter_by(id=client_id)
        ).scalar_one()
        return jsonify(client.to_json()), 200

    @app.route("/parkings", methods=["GET"])
    def get_parkings_handler():
        """
        Список парковок и их состояние.
        В задании не было, сделал для себя, для отслеживания
        """
        parkings: List[Parking] = db.session.execute(db.select(Parking)).scalars()

        parkings_list = [u.to_json() for u in parkings]
        return jsonify(parkings_list), 200

    @app.route("/parkings", methods=["POST"])
    def create_parking_handler():
        """Создание новой парковки"""
        address = request.form.get("address", type=str)
        opened = request.form.get("opened", type=bool)
        count_places = request.form.get("count_places", type=int)
        count_available_places = request.form.get("count_available_places", type=int)

        new_parking = Parking(
            address=address,
            opened=opened,
            count_places=count_places,
            count_available_places=count_available_places,
        )

        db.session.add(new_parking)
        db.session.commit()

        return "", 201

    @app.route("/client_parkings", methods=["POST"])
    def create_client_parkings_handler():
        """Регистрация заезда на парковку"""
        client_id = request.form.get("client_id", type=int)
        parking_id = request.form.get("parking_id", type=int)

        parking = db.session.get(Parking, parking_id)
        if not parking:
            return "Парковка не найдена", 403

        if not parking.opened:
            return "Парковка закрыта", 403

        if parking.count_available_places <= 0:
            return "На парковке нет свободных мест", 403

        card_exists = db.session.get(Client, client_id).credit_card

        if not card_exists:
            return "Карта не привязана,невозможна", 403

        new_parking = ClientParking(
            client_id=client_id, parking_id=parking_id, time_in=datetime.now()
        )

        parking.count_available_places = parking.count_available_places - 1

        db.session.add(new_parking)
        db.session.commit()

        return "", 201

    @app.route("/client_parkings", methods=["DELETE"])
    def delete_client_parkings_handler():
        """Регистрация выезда с парковки"""
        client_id = request.form.get("client_id", type=int)
        parking_id = request.form.get("parking_id", type=int)

        client_parking = db.session.execute(
            db.select(ClientParking).filter_by(
                client_id=client_id, parking_id=parking_id
            )
        ).scalar_one()

        if not client_parking:
            return "Парковка не найдена", 403

        client_parking.time_out = datetime.now()

        parking = db.session.get(Parking, client_parking.parking_id)

        parking.count_available_places = parking.count_available_places + 1

        db.session.commit()

        return "", 202

    return app
